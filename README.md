# docker-README
# Creating a new registry service
PS C:\Users\mtong8\Documents\repos> docker run -d -p 5000:5000 --name registry registry:2
2: Pulling from library/registry
ddad3d7c1e96: Already exists
6eda6749503f: Pull complete
363ab70c2143: Pull complete
5b94580856e6: Pull complete
12008541203a: Pull complete

# Pulling an ubuntu image out of thin air!
PS C:\Users\mtong8\Documents\repos> docker pull ubuntu
Using default tag: latest
latest: Pulling from library/ubuntu
Digest: sha256:adf73ca014822ad8237623d388cedf4d5346aa72c270c5acc01431cc93e18e2d
Status: Image is up to date for ubuntu:latest
docker.io/library/ubuntu:latest

# User must run this command to push image into local service
PS C:\Users\mtong8\Documents\repos> docker push localhost:5000/myfirstpain
Using default tag: latest
The push refers to repository [localhost:5000/myfirstpain]
2f140462f3bc: Pushed
63c99163f472: Pushed
ccdbb80308cc: Pushed
latest: digest: sha256:86ac87f73641c920fb42cc9612d4fb57b5626b56ea2a19b894d0673fd5b4f2e9 size: 943

# If needed, we can simply pull the image after its been pushed
PS C:\Users\mtong8\Documents\repos> docker pull localhost:5000/myfirstpain
Using default tag: latest
latest: Pulling from myfirstpain
Digest: sha256:86ac87f73641c920fb42cc9612d4fb57b5626b56ea2a19b894d0673fd5b4f2e9
Status: Image is up to date for localhost:5000/myfirstpain:latest
localhost:5000/myfirstpain:latest
PS C:\Users\mtong8\Documents\repos>
